<?php

namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class ContactForm extends Form
{

    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField('Nom', 'string')
            ->addField('Prenom', ['type' => 'string'])
            ->addField('Email', ['type' => 'string'])
            ->addField('Message', ['type' => 'text']);
    }

    protected function _buildValidator(Validator $validator)
    {
        return $validator->add('Nom', 'length', [
            'rule' => ['minLength', 2],
            'message' => 'Un nom est requis'
        ])->add('Prenom', 'length',[
            'rule' => ['minLength', 2],
            'message' => 'Un prenom est requis'
        ])->add('Email', 'format', [
            'rule' => 'email',
            'message' => 'Une adresse email valide est requise'  
        ]);
    }

    protected function _execute(array $data)
    {
        
        return true;
    }

}



