<?php

namespace App\Controller;

class ConfigsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['edit']);
    }

    public function edit() {

        // POST = Envoi du formulaire avec les modifications
        if ($this->request->is(['POST', 'PUT'])) {
            //debug($this->request->getData());die();

            $data = $this->request->getData();
            $media = $data['logo'];
            //debug($media); 

            if ( !empty( $media ) ) {

                // Charge la table Medias, permet d'avoir accès à des fonctions du type : $this->Medias->save();
                $this->loadModel('Medias');
                //debug($this->request->getData('logo')); die();
            // debug($media); die();
                $pathinfo = pathinfo( $media['name'] );
                //debug($pathinfo); 

               // renvoie l'extension du fichier en minuscule
                $extension = strtolower($pathinfo['extension']);
               //debug($extension); die();

                // Tableau des différents types d'images
                $is_img = ['jpg', 'png', 'jpeg', 'gif', 'bmp'];

                // Tableau des différents types d'images
                 if (in_array($extension, $is_img)) {


                    // variable data
                    $data = [
                        'NAME' => $media['name'],
                        'ALT' => $media['name'],
                        'TYPE' => 'image',
                        'POSITION' => 0,
                        'PATH' => 'img' . DS . $media['name'],
                        'DRAFT' => 0,
                        'USER_ID' => 0, // A changer avec $this->Auth->user('id')
                        'BLOCK_ID' => null,
                        'SYSTEM' => 'logo'
                    ];

                    // recherche du premier logo existant
                    $logo = $this->Medias->find()->where(['SYSTEM' => 'logo'])->first();

                    // si $logo existe en BDD
                    if ($logo != null) {

                        // Chemin vers le fichier
                        $file_path = WWW_ROOT . $logo->PATH;

                        // Supprime le fichier dans WEBROOT
                        $unlink = unlink($file_path);

                        // Suppression en BDD
                        $result = $this->Medias->delete($logo);

                    }

                    // Enregistrement du fichier dans WEBROOT
                    $new_file =  WWW_ROOT . 'img' . DS . $media['name'];
                    $move = move_uploaded_file($media['tmp_name'], $new_file);

                    if ($move == true) {
                        // alors je crée une new entity
                        $media = $this->Medias->newEntity($data);

                        // sauvegarder les donnees
                        $result = $this->Medias->save($media);
                        // debug($this->request->getData()); die();
                        $this->Flash->success('Sauvegarde réussi');

                    }else{
                        $this->Flash->error('Une erreur est survenue lors de l\'upload du logo.');
                    }

                    // avant de continuer le code, pour &éviter les erreurs, il faut supprimer [ 'logo' => [ ...] ] de $this->request->getData()
                    unset($data['logo']);

                }else{
                    $this->Flash->error('Le Logo doit être un fichier image');
                }

            }


            // formulaire renvois un tableau de tte les data
            foreach ($data as $id => $config_data) {

                // Renvoi l’enregistrement n° $id
                $config = $this->Configs->get($id);
                // debug($config); die();
                
                // Vérifie quels champs ont été modifiés et applique les modifs
                $this->Configs->patchEntity($config, $config_data);

                // sauvegarder les donnees
                $this->Configs->save($config);
            }

            $this->Flash->success('Sauvegarde réussi');

            return $this->redirect($this->referer());

        }else{
            // find serre a trouver les configs et all c'est pour toute les trouvées
            $configs = $this->Configs->find()->all();
            // transmet les données configs a la vue
            $this->set('configs', $configs);

            // Charge la table Medias, permet d'avoir accès à des fonctions du type : $this->Medias->save();
            $this->loadModel('Medias');

            // recherche du premier logo existant
            $logo = $this->Medias->find()->where(['SYSTEM' => 'logo'])->first();

            // transmet les données a la vue
            $this->set('logo', $logo);
        }
    }

  
}
