<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Form\ContactForm;
use Cake\Mailer\Email;

class ContactController extends AppController
{
    public function index(){
        // Utilisation du layout site
        $this->viewBuilder()->layout('site');

        $contact = new ContactForm();

        // POST =  Envoi du message
        if ($this->request->is('post') ) {

            // Validation des données par ContactForm
            if ($contact->execute($this->request->getData())) {
                $this->Flash->success('Nous reviendrons vers vous rapidement.');

                // Ici logique de traitement du mail

                $email = new Email('default');
                $email->from(['me@example.com' => 'My Site'])
                            ->to('pandaas2513@gmail.com')
                            ->subject('About');
                            // ->send('My message');
                debug($email->send('My message')); die();

            // Erreur de validation
            } else {
                $this->Flash->error('Il y a eu un problème lors de la soumission de votre formulaire.');
            }

        // GET = Accès à la page
        }else{

            $this->request->data('Nom', 'Doe');
            $this->request->data('Prenom', 'John');
            $this->request->data('Email', 'john.doe@example.com');
            $this->request->data('Message', 'Bonjour...');

        }
        $this->set('contact', $contact);
    }
}