<?php

namespace App\Controller;

class BlocksController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['block1', 'block3']);
    }
    

   

    public function block1()
    {
        // POST = Envoi du formulaire avec les modifications
        if ($this->request->is(['POST', 'PUT'])) {
            

            $data = $this->request->getData();
            //debug($data); die();

            $media = $data['images'];
           //debug($media); die();

            if (!empty($media)) {
                
                // Charge la table Medias, permet d'avoir accès à des fonctions du type : $this->Medias->save();
                $this->loadModel('Medias');
                

                $pathinfo = pathinfo($media['name']);
               //debug($pathinfo); die();

                // renvoie l'extension du fichier en minuscule
                $extension = strtolower($pathinfo['extension']);
                //debug($extension); die();

                // Tableau des différents types d'images
                $is_img = ['jpg', 'png', 'jpeg', 'gif', 'bmp'];
                
                // Tableau des différents types d'images

                if (in_array($extension, $is_img)) {

                    // variable data
                    $dataImage = [
                        'NAME' => $media['name'],
                        'ALT' => $media['name'],
                        'TYPE' => 'image',
                        'POSITION' =>  $this->request->data('Image1'),
                        'PATH' => 'img' . DS . $media['name'],
                        'DRAFT' => 0,
                        'USER_ID' => 0, // A changer avec $this->Auth->user('id')
                        'BLOCK_ID' => 1,
                        
                    ];
                   // debug($dataImage); die();

                    // recherche de la premiere images existantes
                    $image = $this->Medias->find()->where(['BLOCK_ID' => 1])->first();
                    //debug($image); die();

                    // si $image existe en BDD
                    if ($image != null) {

                        // Chemin vers le fichier
                        $file_path = WWW_ROOT . $image->PATH;

                        // Supprime le fichier dans WEBROOT
                        $unlink = unlink($file_path);

                        // Suppression en BDD
                        $result = $this->Medias->delete($image);

                    }
                    // Enregistrement du fichier dans WEBROOT
                    $new_file =  WWW_ROOT . 'img' . DS . $media['name'];
                    //debug($new_file);die();

                    $move = move_uploaded_file($media['tmp_name'], $new_file);
                    //debug($move); die();

                    if ($move == true) {

                        // alors je crée une new entity
                        $media = $this->Medias->newEntity($dataImage);
                        //debug($media); die();

                        // sauvegarder les donnees
                        $result = $this->Medias->save($media);
                        //debug($result); die();

                        // debug($this->request->getData()); die();
                        $this->Flash->success('Sauvegarde réussi');

                    } else {
                        $this->Flash->error('Une erreur est survenue lors de l\'upload de l\'image.');
                    }


                } else {
                    $this->Flash->error('L\'image doit être un fichier image');
                }
            }
            
            $block1 = $this->Blocks->find()->where(['ID' => 12])->first();
            //debug($block1); die();

            // avant de continuer le code, pour &éviter les erreurs, il faut supprimer [ 'logo' => [ ...] ] de $this->request->getData()
            unset($data['images']);
            
            $dataBlock = [
                'TYPE' => 'block1',
                'NAME' => 'block1',
                'CONTENT' => json_encode($data),
                'DRAFT' => 0,
                'USER_ID' => 0,
            ];
           //debug($dataBlock); die();

            if (empty($block1)) {
                
                $block1 = $this->Blocks->newEntity($dataBlock);
                //debug($block1); die();

                $this->Blocks->save($block1);
                //debug($block);die();

                $this->Flash->success('Sauvegarde réussi');

            } else { 

                $this->Blocks->patchEntity($block1, $dataBlock);
                // debug($this->Blocks->patchEntity($block1, $dataBlock)); die();

                // sauvegarder les donnees
                $this->Blocks->save($block1);

                $this->Flash->success('Sauvegarde réussi');
            }


            return $this->redirect($this->referer());

        } else {
            // find serre a trouver les blocks1 et all c'est pour toute les trouvées
            $blocks = $this->Blocks->find()->where(['ID' => 12])->first();
            
           
            // transmet les données configs a la vue
            $this->set('blocks', $blocks);

            // Charge la table Medias, permet d'avoir accès à des fonctions du type : $this->Medias->save();
            $this->loadModel('Medias');

            // recherche de la premiere image existante
            $image = $this->Medias->find()->where(['BLOCK_ID' => 1])->first();

            // transmet les données a la vue
            $this->set('image', $image);
        }
    }

    public function block2()
    {
    }

    public function block3()
    {
        // POST = Envoi du formulaire avec les modifications
        if ($this->request->is(['POST', 'PUT'])) {


            $data = $this->request->getData();
           //debug($data); die();

            $medias = $data['images'];
            //debug($medias); die();

            foreach ($medias as $media ) {

                if (!empty($media)) {

                    // Charge la table Medias, permet d'avoir accès à des fonctions du type : $this->Medias->save();
                    $this->loadModel('Medias');


                    $pathinfo = pathinfo($media['name']);
                    //debug($pathinfo); die();

                    // renvoie l'extension du fichier en minuscule
                    $extension = strtolower($pathinfo['extension']);
                    //debug($extension); die();

                    // Tableau des différents types d'images
                    $is_img = ['jpg', 'png', 'jpeg', 'gif', 'bmp'];

                    // Tableau des différents types d'images

                    if (in_array($extension, $is_img)) {

                        // variable data
                        $dataImage = [
                            'NAME' => $media['name'],
                            'ALT' => $media['name'],
                            'TYPE' => 'image',
                            'POSITION' =>  $this->request->data('Image1'),
                            'PATH' => 'img' . DS . $media['name'],
                            'DRAFT' => 0,
                            'USER_ID' => 0, // A changer avec $this->Auth->user('id')
                            'BLOCK_ID' => 1,

                        ];
                        // debug($dataImage); die();

                        // recherche de la premiere images existantes
                        $image = $this->Medias->find()->where(['BLOCK_ID' => 3])->first();
                        //debug($image); die();

                        // si $image existe en BDD
                        if ($image != null) {

                            // Chemin vers le fichier
                            $file_path = WWW_ROOT . $image->PATH;

                            // Supprime le fichier dans WEBROOT
                            $unlink = unlink($file_path);

                            // Suppression en BDD
                            $result = $this->Medias->delete($image);
                        }
                        // Enregistrement du fichier dans WEBROOT
                        $new_file =  WWW_ROOT . 'img' . DS . $media['name'];
                        //debug($new_file);die();

                        $move = move_uploaded_file($media['tmp_name'], $new_file);
                        //debug($move); die();

                        if ($move == true) {

                            // alors je crée une new entity
                            $media = $this->Medias->newEntity($dataImage);
                            //debug($media); die();

                            // sauvegarder les donnees
                            $result = $this->Medias->save($media);
                            //debug($result); die();

                            // debug($this->request->getData()); die();
                            $this->Flash->success('Sauvegarde réussi');
                        } else {
                            $this->Flash->error('Une erreur est survenue lors de l\'upload de l\'image.');
                        }
                    } else {
                        $this->Flash->error('L\'image doit être un fichier image');
                    }
                }
            }
            
  
        } else {
            

            // Charge la table Medias, permet d'avoir accès à des fonctions du type : $this->Medias->save();
            $this->loadModel('Medias');

            // recherche de la premiere image existante
            $image = $this->Medias->find()->where(['BLOCK_ID' => 3])->first();

            // transmet les données a la vue
            $this->set('image', $image);
        }
        
        
    }

    public function block4()
    {
    }

    public function blockEntreSsection1()
    {
    }

    public function blockEntreSection2()
    {
    }

}