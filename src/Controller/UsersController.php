<?php

namespace App\Controller;

use Cake\Controller\Controller;

class UsersController extends AppController{

    public function initialize(){
        parent::initialize();
        $this->Auth->allow(['logout', 'add']);
    }

    public function login(){
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect([ 'controller' => 'Configs', 'action' => 'index' ]);
            }
            $this->Flash->error('Votre identifiant ou votre mot de passe est incorrect.');
        }
    }

    public function logout(){
        $this->Flash->success('Vous avez été déconnecté.');
        return $this->redirect($this->Auth->logout());
    }


    /**
     * Vue de creation d'un User
     *
     */
    public function add(){
        $user = $this->Users->newEntity();
        if ( $this->request->is([ 'POST', 'PUT']) ) {
            $data = $this->request->getData();
            $data['ACTIVE'] = 1;
            $user = $this->Users->patchEntity($user, $data);
            debug($user);
            if ($this->Users->save($user)) {
                $this->Flash->success('Sauvegarde réussi');
                return $this->redirect(['action' => 'login']);
            }
            return $this->Flash->error('Erreur');
        }
        $this->set('user', $user);
    }



}

?>