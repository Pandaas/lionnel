header -> 1. Nom du site

body -> 2.les horaires: premier jour de la semaine: lundi.
horaire du premier jour de la semaine: 9h00 - 11h00 sur rendez vous.
jours normaux: mardi, mercredi, jeudi, vendredi.
horaires jours normaux: 8h30 - 12h00 / 14h00 - 18h00.
dernier jour de la semaine: samedi.
horaires du dernier jours de la semaine: 8h30 - 12h00 / 14h00 - 16h30 sur rendez vous
3.nom de lionel: Lionel Matocq-Grabot
4.l'adresse postale: rue: 38 Rue Lucie Diemer Duperret.
code postal et ville: 25200 Montbéliard.
5.le telephone: numéro de l'agence: 03 81 91 15 08.
6.l'adresse du site: adresse du site:''.
7.l'adresse email: agence.matocqgrabot@axa.fr.
8.les reférences de Lionel: Orias n°07012687
RC Profeessionnelle n° RC 43356-Garantie Financière n° GFJ 43356
Menbre d'une association agréée, le règlement des honoraires par chèque est accepté
AGAPLB Convention n° 2002041
N°1050846862 MY au fichier des démarcheurs bancaires ou financiers

footer -> 9.liens et icons réseaux sociaux: lien / icon facebook.
lien / icon twitter.
lien / icon linkedin.






mots clés : assurances, assuré, conseils, bénéficiaires, catégories d'assurances, capital assurance, clause, conditions, conditions particulières, contrat, cotisation,
couverture, adresse-localisation, contact, mail, horaires, devis, tarifs, franchise, responsabilité, déclarations

je ne suis pas sur: avenants, avis de sinistres, aléas, antécédents, avis


Dans la table Config 'name' => 'value'

'site_title' => 'Tattoo Assure'
'monday_hours' => '9h - 12h sur RDV / 14h - 18h'
'tuesday_hours' => '8h30 - 12h00 / 14h00 - 18h00'
'wednesday_hours' => '8h30 - 12h00 / 14h00 - 18h00'
'thursday_hours' => '8h30 - 12h00 / 14h00 - 18h00'
'friday_hours' => '8h30 - 12h00 / 14h00 - 18h00'
'saturday_hours' => '8h30 - 12h00 / 14h00 - 16h30 sur RDV'
'etablissement_name' => 'cabinet'
'lionel_name' => ' Lionel Matocq-Grabot'
'rue_name' => '38 Rue Lucie Diemer Duperret'
'cp_ville_name' => 25200 Montbéliard'
'phone' => '03 81 91 15 08'
'site_adresse' => 'www.tattooassure@...com'
'contact_mail' => 'agence.matocqgrabot@axa.fr'
'lionel_reference' => 'Orias n°07012687, RC Profeessionnelle n° RC 43356-Garantie Financière n° GFJ 43356, Membre d'une association agréée, le règlement des honoraires par chèque est accepté, AGAPLB Convention n° 2002041, N°1050846862 MY au fichier des démarcheurs bancaires ou financiers'
'facebook_link' => NULL
'twitter_link' => NULL
'likedin_link' => NULL


....

section1

ligne 5 à 14 => input titre (h1)
input file (image)
input textarea (paragraphe)

ligne 16 à 30 => input titre (h1)
input textarea (paragraphe)
input file (image)
input titre (h1)

(ligne 34 à 38 => input file (image))


section2

ligne 42 à 54 => input titre (h1)
input textarea (paragraphe1)
input textarea (paragraphe2)

(ligne 56 à 110 => *7 input file (image))

ligne 112 à 183 => input titre (h1)
card 1 => input file (image)
input titre (h3)
input textarea (paragraphe)
input lien
card 2 => input file (image)
input titre (h3)
input textarea (paragraphe)
card 3 => input file (image)
input titre (h3)
input lien
card 4 => input file (image)
input titre (h3)
input lien

(ligne 187 à 191 => input file (image))

section3

ligne 195 à 228 => input file (image)
input titre (h1)
*6 => input text (jour de la semaine)
input text (heures d'ouverture)
input lien (bouton revenir en haut)



<?php

namespace App\Controller;

class ConfigsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['edit']);
    }

    public function edit()
    {


        // POST = Envoi du formulaire avec les modifications
        if ($this->request->is(['POST', 'PUT'])) {
            //debug($this->request->getData());die();


            // Charge la table Medias, permet d'avoir accès à des fonctions du type : $this->Medias->save();
            $this->loadModel('Medias');

            // recherche du premier logo existant
            $logo = $this->Medias->find()->where(['SYSTEM' => 'logo'])->first();
            // debug($logo); die();

            // si $logo existe en BDD
            if ($logo != null) {

                // Chemin vers le fichier
                $file_path = WWW_ROOT . $logo->PATH;


                // Supprime le fichier dans WEBROOT
                $unlink = unlink($file_path);

                // Suppression en BDD
                $result = $this->Medias->delete($logo);
            }



            // Enregistrement du fichier dans WEBROOT
            $media = $this->request->getData('logo');
            // debug($media); die();
            $new_file =  WWW_ROOT . 'img' . DS . $media['name'];
            $move = move_uploaded_file($media['tmp_name'], $new_file);

            // si la variable move est vrai
            if ($move == true) {

                // Retourne des informations sur un chemin
                $pathinfo = pathinfo($new_file);
                // renvoie l'extension du fichier en minuscule
                $extension = strtolower($pathinfo['extension']);
                // Tableau des différents types d'images
                $is_img = ['jpg', 'png', 'jpeg', 'gif', 'bmp'];

                // On vérifie qu'on nous a bien envoyé une image
                if (in_array($extension, $is_img)) {

                    // variable data
                    $data = [
                        'NAME' => $media['name'],
                        'ALT' => $media['name'],
                        'TYPE' => 'image',
                        'POSITION' => 0,
                        'PATH' => 'img' . DS . $media['name'],
                        'DRAFT' => 0,
                        'USER_ID' => 0, // A changer avec $this->Auth->user('id')
                        'SECTION_ID' => null,
                        'SYSTEM' => 'logo'
                    ];

                    // alors je crée une new entity
                    $media = $this->Medias->newEntity($data);

                    // sauvegarder les donnees
                    $result = $this->Medias->save($media);
                    // debug($this->request->getData()); die();

                    // avant de continuer le code, pour &éviter les erreurs, il faut supprimer [ 'logo' => [ ...] ] de $this->request->getData()
                    $data = $this->request->getData();
                    unset($data['logo']);





                    // formulaire renvois un tableau de tte les data
                    foreach ($data as $id => $config_data) {

                        // Renvoi l’enregistrement n° $id
                        $config = $this->Configs->get($id);

                        // Vérifie quels champs ont été modifiés et applique les modifs
                        $this->Configs->patchEntity($config, $config_data);

                        // sauvegarder les donnees
                        $this->Configs->save($config);
                    }

                    $this->Flash->success('Sauvegarde réussi');
                } else {
                    $this->Flash->error('Le Logo doit être un fichier image');
                }
            } else {
                $this->Flash->error('Une erreur est survenue lors de l\'upload du logo.');
            }

            return $this->redirect($this->referer());

            // GET = Accès au formulaire préalablement rempli avec les valeurs en base
        } else {
            // find serre a trouver les configs et all c'est pour toute les trouvées
            $configs = $this->Configs->find()->all();
            // transmet les données configs a la vue
            $this->set('configs', $configs);

            // Charge la table Medias, permet d'avoir accès à des fonctions du type : $this->Medias->save();
            $this->loadModel('Medias');

            // recherche du premier logo existant
            $logo = $this->Medias->find()->where(['SYSTEM' => 'logo'])->first();

            // transmet les données a la vue
            $this->set('logo', $logo);
        }
    }

    public function index()
    {
    }
}
