<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>

<!DOCTYPE html>
<html>

<head>
	<?= $this->Html->charset() ?>

	<title>
		<?= $this->fetch('title') ?>
	</title>

	<?= $this->Html->meta('icon') ?>
	<?= $this->Html->css('style.css') ?>
	<?= $this->Html->css('monStyle.css') ?>
	<?= $this->Html->css('monStyleGrand.css') ?>
	<?= $this->Html->css('responsive.css') ?>

	<?= $this->fetch('meta') ?>
	<?= $this->fetch('css') ?>

</head>

<body>
	<?php
	// Appel à Template/Element/header.ctp
	echo $this->element('header');

	// Affichage des messages Flash cf. UsersController ligne 43 ou 46
	echo $this->Flash->render();

	// Affichage du contenu des views par exemple Template/Users/add.ctp sera affiché à travers cette fonction
	echo $this->fetch('content');

	// Appel à Template/Element/footer.ctp
	echo $this->element('footer');
	?>
</body>
<?= $this->fetch('script'); ?>

</html>