<footer>
    <div class="grid-footer secfoot">
        <div class="footer1 footerrespon1">
            <div class="gris stylerespon">
                <i class="fab fa-facebook"></i>
            </div>
            <div class="gris stylerespon">
                <i class="fab fa-twitter"></i>
            </div>
            <div class="gris stylerespon">
                <i class="fab fa-linkedin-in"></i>
            </div>
        </div>
        <div class="footer2 footerrespon2">
            <a class="gris taille-a-respon" href="#">Mentions Légales</a>
            <a class="gris taille-a-respon" href="#">Politique de Confidentialite</a>
        </div>
    </div>
</footer>