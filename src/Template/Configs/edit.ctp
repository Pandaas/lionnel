<!-- début du formulaire de contact -->
<?php
echo $this->Form->create('Configs', ['enctype' => 'multipart/form-data']);
//debug( $configs );


// Boucle sur toutes les configs
foreach ($configs as $key => $config) {
	//debug($config);
	echo $this->Form->control($config['ID'] . '.VALUE', ['label' => $config['NAME'], 'value' => $config['VALUE']]);
}

// condition si il y a un logo l'afficher
if ($logo != null) {
	echo "<strong>Logo Actuel</strong><br/>";
	echo $this->Html->image($logo['NAME'], ['alt' => $logo['ALT']]);
	echo "<br/>";
}

echo "<strong>Ajouter/Modifier Logo :</strong><br/>";

// Input File
echo $this->Form->file('logo');

// BOUTON ENVOYER 
echo $this->Form->submit('Envoyer', ['class' => 'button']);

//fin du formulaire de contact
echo $this->Form->end();

?>

