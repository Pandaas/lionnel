<!-- DEBUT DE LA PREMIERE SECTION -->

<h1 class="contacttitre">Nous Contactez</h1>
<div class="grid-contact2">
    <!-- début de la première carte de visite -->
    <div class="card card1 front grid-parasec06" data-aos="fade-right">
        <div class="grisclair"></div>
        <div class="gristresclair"></div>
        <div class="gris"></div>
        <div class="dots"></div>
        <div class="personal-intro">
            <p>Assurance</p>
            <p>Lionel Matocq-Grabot</p>
        </div>
    </div>
    <!-- fin de la première carte de visite -->
    <!-- début de la deuxième carte de visite -->
    <div class="card card1 back grid-parasec06" data-aos="zoom-in">
        <div class="yellow"></div>
        <div class="top dots"></div>
        <div class="personal-info">
            <p>123 Address St</p>
            <p>Sacramento, CA 14234</p>
            <p>03 819 115 08 / 0 621 748 422</p>
            <p>www.kristastone.com</p>
            <p>Lassurance@orange.fr</p>
        </div>
        <div class="bottom dots"></div>
        <div class="pink"></div>
    </div>
    <!-- fin de la deuxième carte de visite -->
    <!-- début du texte professionnel -->
    <div class="grid-parasec05 contactrespon" data-aos="fade-left">
        <p class="p-contact-respon">Orias n°07012687<br /> RC Profeessionnelle n° RC 43356-Garantie Financière n° GFJ 43356<br /> Menbre d'une association agréée, le règlement des honoraires par chèque est accepté<br /> AGAPLB Convention n° 2002041
            <br /> N°1050846862 MY au fichier des démarcheurs bancaires ou financiers</p>
    </div>
    <!-- fin du texte professionnel -->
</div>

<!-- FIN DE LA PREMIERE SECTION -->

<!-- Démarcation entre la première section et la deuxième section -->

<div class="contactsimage"></div>

<!-- Fin de la démarcation entre la première section et la deuxième section -->

<!-- DEBUT DE LA DEUXIEME SECTION-->

<section class="formulaire2 formulaire3">
    <h1 class="contacttitre">Formulaire de contact</h1>
    <!-- début du formulaire de contact -->
    <?= $this->Form->create( $contact ); ?>
    <h3>Contactez Nous !</h3>
    <!-- NOM -->
    <?= $this->Form->control('Nom', [
        'label' => 'Nom',
        'placeholder' => 'Qui etes-vous ?',
        'templates' => [
            'inputContainer' => '<div class="formulaire">{{content}}</div>'
        ]
    ]); ?>
    <!-- PRENOM -->
    <?= $this->Form->control('Prenom', [
        'label' => 'Prenom',
        'placeholder' => 'Qui etes-vous ?',
        'templates' => [
            'inputContainer' => '<div class="formulaire">{{content}}</div>'
        ]
    ]); ?>
    <!-- MAIL -->
    <?= $this->Form->control('Email', [
        'label' => 'Email',
        'placeholder' => 'Quelle est votre adresse Email ?',
        'templates' => [
            'inputContainer' => '<div class="formulaire">{{content}}</div>'
        ]
    ]); ?>
    <!-- MESSAGE -->
    <div class="formulaire">
        <?= $this->Form->textarea('Message', ['placeholder' => 'Tapez ici votre message']); ?>
    </div>
    <!-- BOUTON ENVOYER -->
    <?= $this->Form->submit('Envoyer', ['class' => 'button']); ?>

    <?= $this->Form->end(); ?>
    <!-- fin du formulaire de contact -->
</section>

<!-- flèche pour ramener en tete de page  -->
<div class="bouton4 bouton">
    <?= $this->Html->link('<i class="fas fa-sort-up fa-5x"></i>', '#debut', ['escape' => false]); ?>
</div>
<!-- fin de la flèche pour ramener en tete de page  -->

<!-- FIN DE LA DEUXIEME SECTION -->

<!-- début de l'animation des textes -->
<script>
    AOS.init();
</script>
<!-- fin de l'animation des textes -->