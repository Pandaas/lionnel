<!-- Troisieme bloques -->

<?php
echo $this->Form->create('Blocks', ['enctype' => 'multipart/form-data']);

// condition si il y a une image l'afficher
/*if ( $ != null) {
    echo "<strong>Photo Actuelle</strong><br/>";
    echo $this->Html->image($media['NAME'], ['alt' => $media['ALT']]);
    echo "<br/>";
}
echo "<strong>Ajouter/Modifier photo :</strong><br/>";*/

// input pour l'image
echo $this->Form->input('images.', [
    'type' => 'file',
    'multiple' => true,
    
    [
        'label' => $media['NAME']
    ]
]);

// bouton envoyer
echo $this->Form->submit('Envoyer', ['class' => 'button']);

//fin du quatrième bloques
echo $this->Form->end();

?>