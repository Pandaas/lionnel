<!-- Deuxième bloques -->

<?php
echo $this->Form->create('Blocks', ['enctype' => 'multipart/form-data']);

// input pour le h1
echo $this->Form->control('title', ['label' => $block[''], 'value' => $block['']]);

// input textarea 1er paragraphe
echo $this->Form->textarea(
    'texte1',
    [
        'label' => $block[''],
        'placeholder' => 'texte1',
        'value' => $block['']
    ]
);

// input textarea 2ieme paragraphe
echo $this->Form->textarea(
    'texte2',
    [
        'label' => $block[''],
        'placeholder' => 'texte2',
        'value' => $block['']
    ]
);

// bouton envoyer
echo $this->Form->submit('Envoyer', ['class' => 'button']);

//fin du Troisième bloques
echo $this->Form->end();

?>