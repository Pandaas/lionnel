<h1 class="margin1">Nos conditions</h1>
<section class="grid-textecondi">
    <!-- DEBUT DE LA PARTIE PHOTO -->
    <div class="grid-condition carousel-items3">
        <!-- photo 1 -->
        <div class="carousel-item3">
            <?= $this->Html->image('condition1.jpg', ['alt' => 'Condition1', 'class' => 'imagecondi']); ?>
        </div>
        <!-- photo 2 -->
        <div class="carousel-item3">
            <?= $this->Html->image('condition2.jpg', ['alt' => 'Condition2', 'class' => 'imagecondi']); ?>
        </div>
        <!-- photo 3 -->
        <div class="carousel-item3">
            <?= $this->Html->image('condition3.jpg', ['alt' => 'Condition3', 'class' => 'imagecondi']); ?>
        </div>
        <!-- photo 4 -->
        <div class="carousel-item3">
            <?= $this->Html->image('condition4.jpg', ['alt' => 'Condition4', 'class' => 'imagecondi']); ?>
        </div>
        <!-- photo 5 -->
        <div class="carousel-item3">
            <?= $this->Html->image('condition5.jpg', ['alt' => 'Condition5', 'class' => 'imagecondi']); ?>
        </div>
        <!-- photo 6 -->
        <div class="carousel-item3">
            <?= $this->Html->image('condition6.jpg', ['alt' => 'Condition6', 'class' => 'imagecondi']); ?>
        </div>
    </div>
    <!-- FIN DE LA PARTIE PHOTO -->

    <!-- DEBUT DE LA PARTIE TEXTE -->
    <div class="texte1 texterespon">
        <!-- titre avec le premier texte -->
        <div class="texte2" data-aos="zoom-in">
            <h5 class="titre-texte">Vous exercez l’activité professionnelle de TATOUEUR, vous déclarez ne pas :</h5>
            <p>Exercer au domicile de vos clients</p>
            <p>Pratiquer :</p>
            <p class="stylecondi">- de tatouages permanents ur une personne mineure, sans le consentement d’une personne titulaire de l’autorité parentale ou de son tuteur (consentement dont vous êtes en mesure d’apporter la preuve pendant 3 ans)</p>
            <p class="stylecondi">- de détatouage</p>
            <p class="stylecondi">- de scarification</p>
            <p class="stylecondi">- d’implants quels qu’ils soient</p>
            <p class="stylecondi">- de maquillage permanent</p>
        </div>
        <!-- titre avec le deuxième texte -->
        <div class="texte2" data-aos="zoom-in">
            <h5 class="titre-texte">Vous déclarez :</h5>
            <p>- avoir subi une formation aux conditions d’hygiène et de salubrité telle que prévue par l’article R 1311-3 du code de la santé publique,</p>
            <p>- que votre activité est déclarée, conformément à l’article R 1311-2 du code de la santé publique , auprès du directeur général de l’agence régionale compétant pour le lieu ou vous exercez</p>
            <p>- utiliser des produits fabriqués exclusivement dans l’union européenne</p>
        </div>
        <!-- titre avec le troisième texte -->
        <div class="texte2" data-aos="zoom-in">
            <h5 class="titre-texte">Et que :</h5>
            <p>- le matériel pénétrant la barrière cutanée ou entrant en contact avec la peau ou la muqueuse du client et les supports directs de ce matériel, sont soit à usage unique et stérile, soit stérilisés avant chaque utilisation, conformément
                aux articles R 1335-1 à R 1335-8, R 1335-13 et R1335-14 du code de la santé publique,</p>
            <p>- l’élimination des déchets produits par votre activité est réalisée conformément aux articles R 1335-1 à R 1335-13 et R 1335-14 du code de la santé publique.</p>
        </div>
    </div>
    <!-- FIN DE LA PARTIE TEXTE -->
</section>

<!-- flèche pour ramener en tete de page  -->
<div class="bouton4 bouton">
    <?= $this->Html->link('<i class="fas fa-sort-up fa-5x"></i>', '#debut', ['escape' => false]); ?>
</div>
<!-- fin de la flèche pour ramener en tete de page  -->

<!-- FIN DE LA TROISIEME SECTION -->

<!-- début de l'animation des textes -->
<script>
    AOS.init();
</script>
<!-- fin de l'animation des textes -->