<!-- DEBUT DE LA PREMIERE SECTION -->

<section>

    <!-- début partie explicative un titre centré et une images + un texte cote à cote -->
    <h1 class=" margin">Marre de ne pas trouver d'assurance assure ?</h1>
    <div class="grid-marre">
        <?= $this->Html->image('marre2.jpg', ['alt' => 'Marre', 'class' => 'grid-photo']); ?>
        <p class="grid-parasec01" data-aos="fade-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, recusandae, at, labore velit eligendi amet nobis repellat natus sequi sint consectetur excepturi doloribus vero provident consequuntur accusamus quisquam nesciunt cupiditate
            soluta alias illo et deleniti voluptates facilis repudiandae similique dolore quaerat architecto perspiciatis officiis dolor ullam expedita suscipit neque minima rem praesentium inventore ab officia quos dignissimos esse quam placeat iste
            porro eius! Minus, aspernatur nesciunt consectetur.
        </p>
    </div>
    <!-- fin partie explicative images + texte  -->

    <!-- début partie explicative un titre centré et un texte + une image cote à cote -->
    <h1 class="sec02 margin">Vos dossiers sont refusés ?</h1>
    <div class="grid-dossier">
        <p class="grid-parasec01" data-aos="fade-right">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, recusandae, at, labore velit eligendi amet nobis repellat natus sequi sint consectetur excepturi doloribus vero provident consequuntur accusamus quisquam nesciunt cupiditate
            soluta alias illo et deleniti voluptates facilis repudiandae similique dolore quaerat architecto perspiciatis officiis dolor ullam expedita suscipit neque minima rem praesentium inventore ab officia quos dignissimos esse quam placeat iste
            porro eius! Minus, aspernatur nesciunt consectetur.
        </p>
        <?= $this->Html->image('dossier2.jpg', ['alt' => 'Dossier', 'class' => 'grid-photo']); ?>
    </div>
    <!-- fin partie explicative texte + image  -->

    <h1 class="margin">Venez chez nous !!</h1>
</section>

<!-- FIN DE LA PREMIERE SECTION -->



<!-- Démarcation entre la première section et la deuxième section -->

<div class="voiture"></div>

<!-- Fin de la démarcation entre la première section et la deuxième section -->



<!-- DEBUT DE LA DEUXIEME SECTION-->

<!-- début du texte explicatif avec un titre avant le carousel  -->
<section>
    <h1 class="sec02 margin1 margin2">Les Salon de Tatouages</h1>
    <div class="grid-voiture">
        <p class="grid-parasec01 " data-aos="fade-up">Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux
            de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié</p>
        <p class="grid-parasec01 " data-aos="fade-down">Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux
            de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié</p>
    </div>
</section>
<!-- fin du texte explicatif avec un titre avant le carousel  -->

<!-- Début du carousel de 7 images -->
<section class="section_2">
    <ul class="carousel-items carousel-items1">
        <!-- début carousel de l'image 1  -->
        <li class="carousel-item carousel-item1">
            <div class="card">
                <?= $this->Html->image('galerie1.jpg', ['alt' => 'Galerie1', 'class' => 'imgcarousel']); ?>
            </div>
        </li>
        <!-- fin carousel de l'image 1  -->
        <!-- début carousel de l'image 2  -->
        <li class="carousel-item carousel-item1">
            <div class="card">
                <?= $this->Html->image('galerie2.jpg', ['alt' => 'Galerie2', 'class' => 'imgcarousel']); ?>
            </div>
        </li>
        <!-- fin carousel de l'image 2  -->
        <!-- début carousel de l'image 3   -->
        <li class="carousel-item carousel-item1">
            <div class="card">
                <?= $this->Html->image('galerie3.jpg', ['alt' => 'Galerie3', 'class' => 'imgcarousel']); ?>
            </div>
        </li>
        <!-- fin carousel de l'image 3  -->
        <!-- début carousel de l'image 4  -->
        <li class="carousel-item carousel-item1">
            <div class="card">
                <?= $this->Html->image('galerie4.jpg', ['alt' => 'Galerie4', 'class' => 'imgcarousel']); ?>
            </div>
        </li>
        <!-- fin carousel de l'image 4  -->
        <!-- début carousel de l'image 5  -->
        <li class="carousel-item carousel-item1">
            <div class="card">
                <?= $this->Html->image('galerie5.jpg', ['alt' => 'Galerie5', 'class' => 'imgcarousel']); ?>
            </div>
        </li>
        <!-- fin carousel de l'image 5  -->
        <!-- début carousel de l'image 6  -->
        <li class="carousel-item carousel-item1">
            <div class="card">
                <?= $this->Html->image('galerie6.jpg', ['alt' => 'Galerie6', 'class' => 'imgcarousel']); ?>
            </div>
        </li>
        <!-- fin carousel de l'image 6  -->
        <!-- début carousel de l'image 7  -->
        <li class="carousel-item carousel-item1">
            <div class="card">
                <?= $this->Html->image('galerie7.jpg', ['alt' => 'Galerie7', 'class' => 'imgcarousel']); ?>
            </div>
        </li>
        <!-- fin carousel de l'image 7  -->
    </ul>
</section>
<!-- Fin du carousel de 7 images -->

<!-- Début des 4 cards avec un titre et les liens -->
<section class="card-partie-respons">
    <h1 class="margin1">Quelques conseils</h1>
    <div class="section2 sectionrespon1">
        <!-- début de la card 1  -->
        <div class=" card card-respons">
            <div class="card-image ">
                <?= $this->Html->image('sec5-1.jpg', ['alt' => 'Sec5-1', 'class' => 'card-image2']); ?>
            </div>
            <div class="card-body card-body1">
                <div class="card-title">
                    <h3 class="h3-respon">Lorem Ipsum</h3>
                </div>
                <div class="card-excerpt card-excerptrespons">
                    <p class="conseil-respon"> Votre responsabilite civile professionnelle à 21 € ttc par mois ! Garanties 9 000 000 € suivant
                        <?= $this->Html->link('détail*', ''); ?> et <?php echo $this->Html->link('conditions*', ''); ?></p>
                </div>
            </div>
        </div>
        <!-- fin de la card 1  -->
        <!-- début de la card 2  -->
        <div class="card card-respons">
            <div class="card-image">
                <?= $this->Html->image('sec5-2.jpg', ['alt' => 'Sec5-2', 'class' => 'card-image2']); ?>
            </div>
            <div class="card-body card-body1">
                <div class="card-title">
                    <h3 class="h3-respon">Lorem Ipsum</h3>
                </div>
                <div class="card-excerpt card-excerptrespons">
                    <p class="conseil-respon"> Le plus de l’assurance des tatoueurs (sans surprime) mon matériel assuré en tous lieux</p>
                </div>
            </div>
        </div>
        <!-- fin de la card 2  -->
    </div>
    <div class="section2 ajustement section2-respons">
        <!-- début de la card 3  -->
        <div class="card card-respons">
            <div class="card-image">
                <?= $this->Html->image('sec5-5.jpg', ['alt' => 'Sec5-5', 'class' => 'card-image2']); ?>
            </div>
            <div class="card-body card-body1">
                <div class="card-title">
                    <h3 class="h3-respon">Lorem Ipsum</h3>
                </div>
                <div class="card-excerpt">
                    <p class="conseil-respon"><?= $this->Html->link('Demandez nous un devis', ''); ?></p>
                </div>
            </div>
        </div>
        <!-- fin de la card 3  -->
        <!-- début de la card 4  -->
        <div class="card card-respons">
            <div class="card-image">
                <?= $this->Html->image('sec5-4.jpg', ['alt' => 'Sec5-4', 'class' => 'card-image2']); ?>
            </div>
            <div class="card-body card-body1">
                <div class="card-title">
                    <h3 class="h3-respon">Lorem Ipsum</h3>
                </div>
                <div class="card-excerpt">
                    <p class="conseil-respon"><?= $this->Html->link('Et le reste ???', ''); ?></p>
                </div>
            </div>
        </div>
        <!-- fin de la card 4  -->
    </div>
</section>
<!-- Fin des 4 cards avec un titre et les liens -->

<!-- FIN DE LA DEUXIEME SECTION -->



<!-- Démarcation entre la deuxième section et la troisième section -->

<div class="horaire1"></div>

<!-- Fin de la démarcation entre la deuxième section et la troisième section -->



<!-- DEBUT DE LA TROISIEME SECTION -->

<section class="photohor">
    <!-- début de l'animation de l'image des horaires -->
    <div class="box">
        <div class="horaire">
            <div class="imgBx1 imgBx">
                <?= $this->Html->image('open.jpeg', ['alt' => 'Open']); ?>
            </div>
            <div class="details">
                <h1>Horaires</h1>
            </div> 
        </div>
    </div>
    <!-- fin de l'animation de l'image des horaires -->
    <!-- début horaires  -->
    <div class="horaire3 horaire4">
        <p class="couleurhor">Lundi :</p>
        <p> 9h00 - 11h00 sur RDV / 14h00 - 18h00</p>
        <p class="couleurhor2">Mardi / Mercredi / Jeudi / Vendredi :</p>
        <p> 8h30 - 12h00 / 14h00 - 18h00</p>
        <p class="couleurhor">Samedi : </p>
        <p>8h30 - 12h00 / 14h00 - 16h30 sur RDV</p>
    </div>
    <!-- fin horaires  -->
</section>

<!-- flèche pour ramener en tete de page  -->
<div class="bouton4 bouton">
    <?= $this->Html->link('<i class="fas fa-sort-up fa-5x"></i>', '#debut', ['escape' => false]); ?>
</div>
<!-- fin de la flèche pour ramener en tete de page  -->

<!-- FIN DE LA TROISIEME SECTION -->

<!-- début de l'animation des textes -->
<script>
    AOS.init();
</script>
<!-- fin de l'animation des textes -->