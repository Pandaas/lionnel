 <!-- PREMIERE SECTION -->
 <section>
     <h1 class="margin1">Tarifs</h1>
     <p class="garantie margin1">Toutes garanties / Responsabilité civile</p>
     <article class="article-1">
         <div class="container-tarif">
             <!-- début de la première card -->
             <div class="card-tarif">
                 <!-- partie du titre de la card -->
                 <div class="face face1">
                     <div class="content-tarif">
                         <h3>Pour tous dommages</h3>
                     </div>
                 </div>
                 <!-- partie du texte de la card -->
                 <div class="face face2">
                     <div class="content-tarif">
                         <p class="p-tarif">Corporels, matériels et immatériels confondus.</p>
                         <p class="p-tarif">9 000 000 € NON-INDEXES par sinistre toutes responsabilités confondues</p>
                     </div>
                 </div>
             </div>
             <!-- fin de la première card -->
             <!-- début de la deuxième card -->
             <div class="card-tarif">
                 <!-- partie du titre de la card -->
                 <div class="face face1">
                     <div class="content-tarif">
                         <p class="p-tarif">Limites d'indemnisation</p>
                         <h3>Sans pouvoir excéder</h3>
                     </div>
                 </div>
                 <!-- partie du texte de la card -->
                 <div class="face face2">
                     <div class="content-tarif">
                         <p class="p-tarif">Pour dommages matériels et immatériels confondus, 2 500 fois l'indice par sinistre</p>
                         <p class="p-tarif">Pour dommages immatériels non consécutifs, 310 fois l'indice par sinistre</p>
                     </div>
                 </div>
             </div>
             <!-- fin de la deuxième card -->
             <!-- début de la troisième card -->
             <div class="card-tarif">
                 <!-- partie du titre de la card -->
                 <div class="face face1">
                     <div class="content-tarif">
                         <h3>Franchise</h3>
                     </div>
                 </div>
                 <!-- partie du texte de la card -->
                 <div class="face face2">
                     <div class="content-tarif">
                         <h5 class="h-tarif">Franchise générale</h5>
                         <p class="p-tarif">Sur dommages matériels et immatériels par sinistre</p>
                         <p class="p-tarif">indiquée aux Conditions particulières</p>
                     </div>
                 </div>
             </div>
             <!-- fin de la troisième card -->
         </div>
     </article>
     <p class="garantie">Sauf en cas *</p>
     <article class="article-1">
         <div class="container-tarif">
             <!-- début de la quatrième card -->
             <div class="card-tarif">
                 <!-- partie du titre de la card -->
                 <div class="face face1">
                     <div class="content-tarif">
                         <h3>*</h3>
                     </div>
                 </div>
                 <!-- partie du texte de la card -->
                 <div class="face face2">
                     <div class="content-tarif">
                         <p class="p-tarif">Dommages à des biens confiés</p>
                         <p class="p-tarif">Intoxication alimentaire et après livraison du produit ou réception de travaux</p>
                     </div>
                 </div>
             </div>
             <!-- fin de la quatrième card -->
             <!-- début de la cinquième card -->
             <div class="card-tarif">
                 <!-- partie du titre de la card -->
                 <div class="face face1">
                     <div class="content-tarif">
                         <h3>Sauf disposition particulières suivantes</h3>
                     </div>
                 </div>
                 <!-- partie du texte de la card -->
                 <div class="face face2">
                     <div class="content-tarif">
                         <p class="p-tarif1">Dommages matériels et immatériels confondu: 310 fois l'indice par sinistre</p>
                         <p class="p-tarif1">Tous dommages corporels, matériels et immatériels confondu: 3 390 fois l'indice par année d'assurance, dont 310 pour les dommages immatériels non consécutifs</p>
                     </div>
                 </div>
             </div>
             <!-- fin de la cinquième card -->
             <!-- début de la sixième card -->
             <div class="card-tarif">
                 <!-- partie du titre de la card -->
                 <div class="face face1">
                     <div class="content-tarif">
                         <h3>Franchise spécifique</h3>
                     </div>
                 </div>
                 <!-- partie du texte de la card -->
                 <div class="face face2">
                     <div class="content-tarif">
                         <p class="p-tarif">10 % des dommages -mini: franchise générale, -maxi: 1.5 fois l'indice</p>
                     </div>
                 </div>
             </div>
             <!-- fin de la sixième card -->
         </div>
     </article>
 </section>
 <!-- FIN DE LA PREMIERE SECTION -->

 <!-- Démarcation entre la première section et la deuxième section -->

 <div class="tarifs1"></div>

 <!-- Fin de la démarcation entre la première section et la deuxième section -->

 <!-- DEBUT DE LA DEUXIEME SECTION-->

 <section>
     <article class="article-1">
         <div class="container-tarif">
             <!-- début de la septième card -->
             <div class="card-tarif">
                 <!-- partie du titre de la card -->
                 <div class="face face1">
                     <div class="content-tarif">
                         <h3>Vol par préposé</h3>
                     </div>
                 </div>
                 <!-- partie du texte de la card -->
                 <div class="face face2">
                     <div class="content-tarif">
                         <p class="p-tarif">Dommages matériels et immatériels confondus: 310 fois l'indice par sinistre</p>
                         <h5 class="h-tarif">Franchise générale</h5>
                         <p class="p-tarif">indiquée aux Conditions particulières</p>
                     </div>
                 </div>
             </div>
             <!-- fin de la septième card -->
             <!-- début de la huitième card -->
             <div class="card-tarif">
                 <!-- partie du titre de la card -->
                 <div class="face face1">
                     <div class="content-tarif">
                         <h3>Faute inexcusable</h3>
                     </div>
                 </div>
                 <!-- partie du texte de la card -->
                 <div class="face face2">
                     <div class="content-tarif">
                         <p class="p-tarif">Tous dommages corporels, matériels et immatériels confondus : 1 000 000 € par sinistre et 2 000 000 € par année d'assurance
                         </p>
                         <h5 class="h-tarif">Franchise générale</h5>
                         <p class="p-tarif">indiquée aux Conditions particulières</p>
                     </div>
                 </div>
             </div>
             <!-- fin de la huitième card -->
             <!-- début de la neuvième card -->
             <div class="card-tarif">
                 <!-- partie du titre de la card -->
                 <div class="face face1">
                     <div class="content-tarif">
                         <h3>Dommages aux biens des préposés</h3>
                     </div>
                 </div>
                 <!-- partie du texte de la card -->
                 <div class="face face2">
                     <div class="content-tarif">
                         <p class="p-tarif">Dommages matériels et immatériels confondus: 310 fois l'indice par sinistre</p>
                         <h5 class="h-tarif">Franchise générale</h5>
                         <p class="p-tarif">indiquée aux Conditions particulières</p>
                     </div>
                 </div>
             </div>
             <!-- fin de la neuvième card -->
         </div>
     </article>
     <article class="article-2">
         <div class="container-tarif">
             <!-- début de la dixième card -->
             <div class="card-tarif">
                 <!-- partie du titre de la card -->
                 <div class="face face1">
                     <div class="content-tarif">
                         <h3>Risques environnementaux</h3>
                     </div>
                 </div>
                 <!-- partie du texte de la card -->
                 <div class="face face2">
                     <div class="content-tarif">
                         <p class="p-tarif">1 000 000 € par année d'assurance Responsabilité civile Atteinte à l'environnement accidentelle dont 1 000 000 € par année d'assurance Préjudice écologique et Responsabilité environnementale
                         </p>
                         <h5 class="h-tarif">Franchise générale</h5>
                         <p class="p-tarif">400 € par sinistre</p>
                     </div>
                 </div>
             </div>
             <!-- fin de la dixième card -->
             <!-- début de la onzième card -->
             <div class="card-tarif">
                 <!-- partie du titre de la card -->
                 <div class="face face1">
                     <div class="content-tarif">
                         <h3>Responsabilité des dirigeants</h3>
                     </div>
                 </div>
                 <!-- partie du texte de la card -->
                 <div class="face face2">
                     <div class="content-tarif">
                         <p class="p-tarif">50 000 € NON INDEXES par année d'assurance</p>
                         <h5 class="h-tarif">Franchise générale</h5>
                         <p class="p-tarif">indiquée aux Conditions particulières</p>
                     </div>
                 </div>
             </div>
             <!-- fin de la onzième card -->
         </div>
     </article>
 </section>

 <!-- FIN DE LA DEUXIEME SECTION-->

 <!-- Démarcation entre la deuxième section et la troisième section -->

 <div class="tarifs2"></div>

 <!-- Fin de la démarcation entre la deuxième section et la troisième section -->

 <!-- DEBUT DE LA TROISIEME SECTION -->

 <section>
     <div class="grid-tarif3">
         <?= $this->Html->image('tarif3.jpg', ['alt' => 'Tarif3', 'class' => 'grid-renseignement grid-parasec01 imagetarifrespon']); ?>
         <p class="grid-parasec04 texterespontarif">Pour toutes demandes de renseignements merci de nous contacter soit par courrier, téléphone ou par email à l'aide du formulaire se trouvant dans la section contact, nous vous répondrons dans les plus brefs délais.</p>
     </div>
 </section>

 <!-- flèche pour ramener en tete de page  -->
 <div class="bouton4 bouton">
     <?= $this->Html->link('<i class="fas fa-sort-up fa-5x"></i>', '#debut', ['escape' => false]); ?>
 </div>
 <!-- fin de la flèche pour ramener en tete de page  -->

 <!-- FIN DE LA TROISIEME SECTION -->

 <!-- début de l'animation des textes -->
 <script>
     AOS.init();
 </script>
 <!-- fin de l'animation des textes -->