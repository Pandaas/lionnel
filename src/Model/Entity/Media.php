<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Media extends Entity
{
    protected $_accessible = [
        'NAME' => true,
        'ALT' => true,
        'TYPE' => true,
        'POSITION' => true,
        'PATH' => true,
        'DRAFT' => true,
        'created' => true,
        'modified' => true,
        'USER_ID' => true,
        'BLOCK_ID' => true,
        'SYSTEM' => true,
    ];




}
