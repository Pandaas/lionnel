<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Block extends Entity
{
    protected $_accessible = [
        'TYPE' => true,
        'NAME' => true,
        'CONTENT' => true,
        'DRAFT' => true,
        'USER_ID' => true,
        'created' => true,
        'modified' => true
    ];
}

