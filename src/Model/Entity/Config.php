<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Config extends Entity
{
    protected $_accessible = [
        'NAME' => true,
        'VALUE' => true,
        'DRAFT' => true,
        'created' => true,
        'modified' => true,
        'USER_ID' => true,
    ];
}
