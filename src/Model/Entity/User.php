<?php

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

class User extends Entity
{
    protected $_accessible = [
        'NAME' => true,
        'PASSWORD' => true,
        'EMAIL' => true,
        'ACTIVE' => true,
    ];

    protected function _setPassword($value){
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();

            return $hasher->hash($value);
        }
    }
}
