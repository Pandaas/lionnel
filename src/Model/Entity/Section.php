<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Media extends Entity
{
    protected $_accessible = [
        'TYPE' => true,
        'CONTACT_FORM' => true,
        'POSITION' => true,
        'TITLE' => true,
        'TEXT' => true,
        'DRAFT' => true,
        'created' => true,
        'modified' => true,
        'USER_ID' => true,
        'PAGE_ID' => true,
    ];
}
