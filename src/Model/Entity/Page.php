<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Page Entity
 *
 * @property int $ID
 * @property string|null $TITLE
 * @property string|null $KEYWORDS
 * @property bool $ONLINE
 * @property bool $DRAFT
 * @property \Cake\I18n\FrozenTime $CREATED
 * @property \Cake\I18n\FrozenTime $MODIFIED
 * @property int $USER_ID
 */
class Page extends Entity{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'TITLE' => true,
        'KEYWORDS' => true,
        'ONLINE' => true,
        'DRAFT' => true,
        'CREATED' => true,
        'MODIFIED' => true,
        'USER_ID' => true,
    ];

}
