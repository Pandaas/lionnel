<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table{

    public function initialize(array $config){
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('ID');
        $this->setPrimaryKey('ID');
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmptyString('ID', null, 'create');

        $validator
            ->scalar('NAME')
            ->maxLength('NAME', 255)
            ->allowEmptyString('NAME');

        $validator
            ->scalar('PASSWORD')
            ->maxLength('PASSWORD', 255)
            ->allowEmptyString('PASSWORD');

        $validator
            ->scalar('EMAIL')
            ->maxLength('EMAIL', 255)
            ->allowEmptyString('EMAIL');

        $validator
            ->boolean('ACTIVE')
            ->notEmptyString('ACTIVE');

        return $validator;
    }

}