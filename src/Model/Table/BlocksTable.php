<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


Class BlocksTable extends Table
{

    public function initialize(array $block)
    {
        parent::initialize($block);

        $this->settable('blocks');
        $this->setdisplayField('ID');
        $this->setprimaryKey('ID');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Draft');

        $this->belongsTo('Users');
           /* ->setForeignKey('USER_ID')
            ->setJoinType('INNER'); */
    }


    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmptyString('ID', null, 'create');

        $validator
            ->scalar('NAME')
            ->maxLength('NAME', 255)
            ->allowEmptyString('NAME');

        $validator
            ->scalar('CONTENT')
            ->maxLength('CONTENT', 6500)
            ->allowEmptyString('CONTENT');

        $validator
            ->boolean('DRAFT')
            ->notEmptyString('DRAFT');

        $validator
            ->integer('USER_ID')
            ->requirePresence('USER_ID', 'create')
            ->notEmptyString('USER_ID');

        // $validator
        //     ->dateTime('created')
        //     ->requirePresence('created', 'create')
        //     ->notEmptyDateTime('created');

        // $validator
        //     ->dateTime('modified')
        //     ->requirePresence('modified', 'create')
        //     ->notEmptyDateTime('modified');

        return $validator;
    }
}