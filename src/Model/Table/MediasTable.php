<?php

namespace App\Model\Table; 

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;



class MediasTable extends Table
{
   public function initialize(array $config){
        parent::initialize($config);

        $this->setTable('medias');
        $this->setDisplayField('ID');
        $this->setPrimaryKey('ID');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Draft');

        $this->belongsTo('Users');
           /* ->setForeignKey('USER_ID')
            ->setJoinType('INNER'); */

        // $this->belongsToMany('Sections');
           /* ->setForeignKey('SECTION_ID')
            ->setJoinType('INNER'); */

   }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmptyString('ID', null, 'create');

        $validator
            ->scalar('NAME')
            ->maxLength('NAME', 255)
            ->allowEmptyString('NAME');

        $validator
            ->scalar('ALT')
            ->maxLength('ALT', 255)
            ->allowEmptyString('ALT');

        $validator
            ->scalar('TYPE')
            ->maxLength('TYPE', 255)
            ->allowEmptyString('TYPE');

        $validator
            ->integer('POSITION')
            ->allowEmptyString('POSITION');

        $validator
            ->boolean('DRAFT')
            ->notEmptyString('DRAFT');



        // $validator
        //     ->dateTime('CREATED')
        //     ->requirePresence('CREATED', 'create')
        //     ->notEmptyDateTime('CREATED');

        // $validator
        //     ->dateTime('MODIFIED')
        //     ->requirePresence('MODIFIED', 'create')
        //     ->notEmptyDateTime('MODIFIED');


        $validator
            ->integer('USER_ID')
            ->requirePresence('USER_ID', 'create')
            ->notEmptyString('USER_ID');

        $validator
            ->integer('BLOCK_ID')
            ->allowEmptyString('BLOCK_ID', null);

        $validator
            ->scalar('SYSTEM')
            ->maxLength('SYSTEM', 50)
            ->allowEmptyString('SYSTEM');

        return $validator;
    }
}





