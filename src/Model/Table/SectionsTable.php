<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class SectionsTable extends Table {

    public function initialize(array $config){
        parent::initialize($config);

        $this->setTable('sections');
        $this->setDisplayField('ID');
        $this->setPrimaryKey('ID');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Draft');


        $this->belongsTo('Users');
           /* ->setForeignKey('USER_ID')
            ->setJoinType('INNER'); */

        $this->belongsToMany('Pages');
          /*  ->setForeignKey('PAGES_ID')
            ->setJoinType('INNER'); */

    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmptyString('ID', null, 'create');

        $validator
            ->scalar('TYPE')
            ->maxLength('TYPE', 255)
            ->allowEmptyString('TYPE');

        $validator
            ->scalar('CONTACT_FORM')
            ->maxLength('CONTACT_FORM', 255)
            ->allowEmptyString('CONTACT_FORM');

        $validator
            ->integer('POSITION')
            ->allowEmptyString('POSITION');

        $validator
            ->scalar('TITLE')
            ->maxLength('TITLE', 255)
            ->allowEmptyString('TITLE');

        $validator
            ->scalar('TEXT')
            ->maxLength('TEXT')
            ->allowEmptyString('TEXT');

        $validator
            ->boolean('DRAFT')
            ->notEmptyString('DRAFT');

        $validator
            ->dateTime('CREATED')
            ->requirePresence('CREATED', 'create')
            ->notEmptyDateTime('CREATED');

        $validator
            ->dateTime('MODIFIED')
            ->requirePresence('MODIFIED', 'create')
            ->notEmptyDateTime('MODIFIED');

        $validator
            ->integer('USER_ID')
            ->requirePresence('USER_ID', 'create')
            ->notEmptyString('USER_ID');

        $validator
            ->integer('PAGE_ID')
            ->requirePresence('PAGE_ID', 'create')
            ->notEmptyString('PAGE_ID');

        return $validator;
    }
}