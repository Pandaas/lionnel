<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;



class ConfigsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->settable('configs');
        $this->setdisplayField('ID');
        $this->setprimaryKey('ID');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Draft');

        $this->belongsTo('Users');
           /* ->setForeignKey('USER_ID')
            ->setJoinType('INNER'); */
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmptyString('ID', null, 'create');

        $validator
            ->scalar('NAME')
            ->maxLength('NAME', 255)
            ->allowEmptyString('NAME');

        $validator
            ->scalar('VALUE')
            ->maxLength('VALUE', 255)
            ->allowEmptyString('VALUE');

        $validator
            ->boolean('DRAFT')
            ->notEmptyString('DRAFT');

        // $validator
        //     ->dateTime('CREATED')
        //     ->requirePresence('CREATED', 'create')
        //     ->notEmptyDateTime('CREATED');

        // $validator
        //     ->dateTime('MODIFIED')
        //     ->requirePresence('MODIFIED', 'create')
        //     ->notEmptyDateTime('MODIFIED');

        $validator
            ->integer('USER_ID')
            ->requirePresence('USER_ID', 'create')
            ->notEmptyString('USER_ID');

        return $validator;
    }
}
