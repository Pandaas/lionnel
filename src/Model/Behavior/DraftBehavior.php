<?php
namespace App\Model\Behavior;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;

class DraftBehavior extends Behavior{

    private $options = [
        'conditions' => [ 'draft' => 1 ]
    ];

    public function initialize( array $options ){
        parent::initialize($options);
        $this->options[ $this->_table->alias() ] = array_merge( $this->options, $options );
    }

    public function getDraftId( $conditions = [] ){
        $conditions = array_merge( $this->options[ $this->_table->alias() ]['conditions'], $conditions);
        $ent = $this->_table->newEntity( $conditions );
        $this->_table->save( $ent );
        return $ent->id;
    }

    public function cleanDrafts( $conditions = [] ){
        $yesterday = strtotime('-1 day');
        $date = date('Y/m/d', $yesterday );
        $conditions = array_merge( [ $this->_table->alias().'.draft' => 1, $this->_table->alias().'.created <' => $date ], $conditions );
        return $this->_table->deleteAll( $conditions, false, true );
    }





}
